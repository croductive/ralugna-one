import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  slogan: string = 'Your one stop for the things hi'
  source: string = '/assets/shopping.png'
  getSlogan() {
    return 'Your one stop for the things hi'
  }
}
