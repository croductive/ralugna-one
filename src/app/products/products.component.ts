import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {

  products = 
[{"id":1,"name":"Wine - Mas Chicet Rose, Vintage","price":"$1699.16","color":"Goldenrod","gender":"Male","image":"http://dummyimage.com/158x100.png/dddddd/000000"},
{"id":2,"name":"Brocolinni - Gaylan, Chinese","price":"$8459.08","color":"Fuscia","gender":"Female","image":"http://dummyimage.com/117x100.png/cc0000/ffffff"},
{"id":3,"name":"Rambutan","price":"$6278.95","color":"Crimson","gender":"Male","image":"http://dummyimage.com/147x100.png/ff4444/ffffff"},
{"id":4,"name":"Ginger - Fresh","price":"$7213.51","color":"Red","gender":"Male","image":"http://dummyimage.com/236x100.png/5fa2dd/ffffff"},
{"id":5,"name":"Nantucket - Kiwi Berry Cktl.","price":"$7224.72","color":"Turquoise","gender":"Male","image":"http://dummyimage.com/195x100.png/cc0000/ffffff"},
{"id":6,"name":"V8 Splash Strawberry Kiwi","price":"$4542.11","color":"Blue","gender":"Male","image":"http://dummyimage.com/243x100.png/5fa2dd/ffffff"},
{"id":7,"name":"Lady Fingers","price":"$8681.67","color":"Red","gender":"Female","image":"http://dummyimage.com/128x100.png/cc0000/ffffff"},
{"id":8,"name":"Pork - Shoulder","price":"$3006.22","color":"Maroon","gender":"Female","image":"http://dummyimage.com/210x100.png/dddddd/000000"},
{"id":9,"name":"Sugar - Brown","price":"$8642.67","color":"Pink","gender":"Female","image":"http://dummyimage.com/195x100.png/ff4444/ffffff"},
{"id":10,"name":"Chambord Royal","price":"$3913.06","color":"Puce","gender":"Female","image":"http://dummyimage.com/126x100.png/5fa2dd/ffffff"},
{"id":11,"name":"Sauce - Soy Low Sodium - 3.87l","price":"$48.25","color":"Aquamarine","gender":"Male","image":"http://dummyimage.com/117x100.png/ff4444/ffffff"},
{"id":12,"name":"Pasta - Cheese / Spinach Bauletti","price":"$4029.17","color":"Goldenrod","gender":"Male","image":"http://dummyimage.com/168x100.png/ff4444/ffffff"},
{"id":13,"name":"Lemon Grass","price":"$7223.71","color":"Orange","gender":"Male","image":"http://dummyimage.com/191x100.png/5fa2dd/ffffff"},
{"id":14,"name":"Tomato Paste","price":"$7140.84","color":"Mauv","gender":"Female","image":"http://dummyimage.com/179x100.png/ff4444/ffffff"},
{"id":15,"name":"Container - Clear 32 Oz","price":"$6680.04","color":"Yellow","gender":"Female","image":"http://dummyimage.com/121x100.png/5fa2dd/ffffff"},
{"id":16,"name":"Pumpkin","price":"$6973.14","color":"Orange","gender":"Female","image":"http://dummyimage.com/123x100.png/ff4444/ffffff"},
{"id":17,"name":"Rye Special Old","price":"$3047.34","color":"Mauv","gender":"Male","image":"http://dummyimage.com/192x100.png/cc0000/ffffff"},
{"id":18,"name":"Squash - Pattypan, Yellow","price":"$2561.24","color":"Fuscia","gender":"Female","image":"http://dummyimage.com/129x100.png/cc0000/ffffff"},
{"id":19,"name":"Pasta - Cheese / Spinach Bauletti","price":"$6437.29","color":"Blue","gender":"Male","image":"http://dummyimage.com/114x100.png/dddddd/000000"},
{"id":20,"name":"Cranberries - Frozen","price":"$8760.34","color":"Green","gender":"Male","image":"http://dummyimage.com/188x100.png/ff4444/ffffff"}]
}
